﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlagiarismLibrary
{
    public class PlagiarismAnalyzer
    {
        public bool Analyze(string a, string b)
        {
            var docA = new DocInfo(a);
            var docB = new DocInfo(b);

            List<string> wordsA = new List<string>(docA.WordCounts.Keys);
            wordsA.Sort();
            List<string> wordsB = new List<string>(docB.WordCounts.Keys);
            wordsB.Sort();

            // we want to count the total number of word instances in B that are not in A
            // we want to also count the total number of word instances in B.
            // we want to also know the total number of word instances in A.

            string currentWord;
            int wordsCounted = 0;
            int totalWords = wordsA.Count + wordsB.Count;
            int idxA = 0;
            int idxB = 0;

            int wordCountA = 0;
            int wordCountB = 0;
            int uniqueWordsB = 0;

            currentWord = GetCurrentWord(wordsA, wordsB, idxA, idxB);

            while (wordsCounted < totalWords)
            {
                int aCount = 0;

                if (idxA < wordsA.Count && currentWord == wordsA[idxA])
                {
                    wordCountA += docA.WordCounts[currentWord]; // the count of words in A
                    aCount = docA.WordCounts[currentWord];
                    wordsCounted++;
                    idxA++;
                }

                if (idxB < wordsB.Count && currentWord == wordsB[idxB])
                {
                    int bCount = docB.WordCounts[currentWord];

                    wordCountB += bCount; // count of words in B

                    if (bCount > aCount)
                    {
                        uniqueWordsB += (docB.WordCounts[currentWord] - aCount);
                    }
                    wordsCounted++;
                    idxB++;
                }

                currentWord = GetCurrentWord(wordsA, wordsB, idxA, idxB);
            }

            return (float)uniqueWordsB / (float)wordCountB <= 0.5f;
        }

        private string GetCurrentWord(List<string> a, List<string> b, int idxA, int idxB)
        {
            string currentWord = null;

            if (idxA >= a.Count && idxB >= b.Count)
            {
                return null;
            }

            if (idxA >= a.Count)
            {
                return b[idxB];
            }

            if (idxB >= b.Count)
            {
                return a[idxA];
            }

            if (a[idxA].CompareTo(b[idxB]) <= 0)
            {
                currentWord = a[idxA];
            }
            else
            {
                currentWord = b[idxB];
            }

            return currentWord;
        }
    }
}
