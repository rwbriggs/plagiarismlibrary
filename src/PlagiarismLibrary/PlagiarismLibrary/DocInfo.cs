﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlagiarismLibrary
{
    public class DocInfo
    {
        private readonly string _parsedString;
        private readonly Dictionary<string, string> _smallWords = new Dictionary<string, string>
        {
            {"the", "the"},
            {"and", "and"},
            {"but", "but"},
            {"or", "or"},
            {"re", "re" },
            {"it", "it"},
            {"is", "is"},
            {"not", "not"},
            {"you", "you"},
            {"your", "your" },
            {"they", "they"},
            {"theyre", "theyre"},
            {"are", "are"},
            {"youre", "youre"},
            {"as", "as"},
            {"in", "in"},
            {"of", "of"},
            {"if", "if"},
            {"to", "to" },
            {"too", "too" },
            {"two", "two" }
        };

        private readonly Dictionary<string, int> _wordCounts = new Dictionary<string, int>();

        public DocInfo(string src)
        {
            _parsedString = ParseDocument(src);
        }

        private string ParseDocument(string src)
        {
            var content = new StringBuilder();
            var currWord = new StringBuilder();

            foreach (char c in src)
            {
                if (!CharacterAdded(c, currWord))
                {
                    // only words with 2 or more characters are going to be possibly meaningful
                    AddWord(content, currWord);

                    currWord.Clear();
                }
            }

            AddWord(content, currWord);

            return content.ToString();
        }

        private bool CharacterAdded(char c, StringBuilder word)
        {
            if (c >= 'a' && c <= 'z')
            {
                word.Append(c);
                return true;
            }
            else if (c >= 'A' && c <= 'Z')
            {
                word.Append((char)('a' + (c - 'A')));
                return true;
            }

            return false;
        }

        private void AddWord(StringBuilder content, StringBuilder word)
        {
            if (word.Length > 1 && !_smallWords.ContainsKey(word.ToString()))
            {
                string w = word.ToString();

                if (_wordCounts.ContainsKey(w))
                {
                    _wordCounts[w]++;  // increment count by 1
                }
                else
                {
                    _wordCounts.Add(w, 1); // initialize to count of 1
                }

                content.Append(w);
            }
        }

        public override string ToString()
        {
            return _parsedString;
        }

        public Dictionary<string, int> WordCounts
        {
            get { return _wordCounts; }
        }
    }
}
