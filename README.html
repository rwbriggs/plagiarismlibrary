﻿<!DOCTYPE html>
<html>
<head>
    <title>PlagiarismLibrary</title>
</head>
<body>

    <h1 id="plagiarismlibrary">PlagiarismLibrary</h1>
<h3 id="using-test-driven-development-tdd">Using Test Driven Development (TDD)</h3>
<p>The basic concept of TDD is that you first design a test for the code you have yet to write.  That test should fail. Then you write the actual code that will pass the test.  You continue to add capabilities to your project by first designing the capability, then implement the tests to validate, and finally write the code to pass the tests.  Sounds simple, right?</p>
<p>Here we go.</p>
<p>We will develop a library to determine if the content of one text file is the same as the content of another text file.  The resulting application will be able to detect if one article is plagiarized off of another.  In order to do this, we will need to parse out the unique words and word count for a body of text, ignoring filler words, to generate a histogram of the word counts.  We'll compare this to the histogram for another body of text and use a scoring system to determine if there is a likelihood of plagiarism.</p>
<p>It will be the library we develop the unit tests for.  In the end, we will create a simple console application to see if we succeeded.</p>
<p>I will be using Visual Studio 2017 and C# for this demonstration.  The same principles will apply in any language with support for unit tests.  I will call out the differences as they become apparent to me.  As we go through this process you will likely see that we will have to change our implementation as we introduce new capabilities to the library.  This is normal behavior for TDD, and, in truth, for most development practices.  The point to be made here is that change is something to be embraced and not feared.</p>
<p>So, first steps.  We'll start with a project for a .Net Core library called PlagiarismLibrary.  The main class for that library will be PlagiarismAnalyzer, which will be our main entry point for comparing text and determining if something has just been copied.  So we start with the class as follows:</p>
<pre><code class="language-csharp">    public class PlagiarismAnalyzer
    {
    }
</code></pre>
<p>No, it doesn't do anything yet.  That's fine.  What we need to do is to create the unit test library, so we'll add the PlagiarismLibraryTests XUnit testing project.  In it we'll create our first test class called AnalyzerTests and this is where we'll start.</p>
<pre><code class="language-csharp">    public class AnalyzerTests
    {
    }
</code></pre>
<p>We are still doing a whole lot of nothing so far.  Let's create a test.  The main, basic test will be to make sure we indicate that 2 strings are equal when they are, indeed, equal.  So let's add the following test:</p>
<pre><code class="language-csharp">    public class AnalyzerTests
    {
        [Fact]
        public void SameText_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze(&quot;This is a test&quot;, &quot;This is a test&quot;);

            Assert.True(result, &quot;The result should be true for the same text.&quot;);
        }
    }
</code></pre>
<p>Our first test ('Fact' in XUnit) will analyze 2 strings that are identical and expect a <strong>true</strong> response.  This makes sense, right?  But it won't build and the editor will complain that the function Analyze doesn't exist.  One of the parts of TDD is that you only create what you need.  The test will provide the design.  So, let's build the Analyze method!  This is what the interface should look like:</p>
<pre><code class="language-csharp">    public class PlagiarismAnalyzer
    {
        public bool Analyze(string a, string b)
        {
        }
    }
</code></pre>
<p>We have not returned a value, so it won't compile.  Let's return the default value for a Boolean.  So, we'll have this:</p>
<pre><code class="language-csharp">    public class PlagiarismAnalyzer
    {
        public bool Analyze(string a, string b)
        {
            return default(bool);
        }
    }
</code></pre>
<p>Let's run the test.  It fails.  Which is what we expected, as the default value of a Boolean is false.  So, we need to fix our code to pass the test.  This is our first opportunity to do something!  While we <strong>can</strong> actually compare the strings, that would be too much work.  The goal of TDD is to do the <strong>minimum</strong> amount of work in order to pass the tests.  So we'll change the Analyze() function to:</p>
<pre><code class="language-csharp">        public bool Analyze(string a, string b)
        {
            return true;
        }
</code></pre>
<p>Now, when we run the test it passes.</p>
<p>We added the first test and it passes.  That's a good start!  However, if we create a second test that is supposed to fail, our code will not pass.  The new test needs to make sure that different strings will return false.  Here's the new test:</p>
<pre><code class="language-csharp">        [Fact]
        public void DifferentText_ReturnsFalse()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze(&quot;This test should always fail.&quot;, &quot;Indeed, we like this one.&quot;);

            Assert.False(result, &quot;The different text should always fail.&quot;);
        }
</code></pre>
<p>However, when we run our tests we have a failure.  We knew that was going to happen, right?  So, how do we fix it?  Well, it seems we need to compare the string values.  That's pretty easy to do.</p>
<pre><code class="language-csharp">        public bool Analyze(string a, string b)
        {
            return a == b;
        }
</code></pre>
<p>Now, when we run our tests they pass.  It's a nice start.  We are not done yet.  We only know that if two strings are identical will they be flagged as plagiarized copies.  We need to up our game.  The next test is to insert some extra spaces in the text to be tested.  The expectation is that it should still return true.</p>
<pre><code class="language-csharp">        [Fact]
        public void SameTextWithSpaces_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze(&quot;This  is   a   test&quot;, &quot;  This is a test   &quot;);

            Assert.True(result, &quot;The result should be true for the same words in the text.&quot;);
        }
</code></pre>
<p>This test will fail.  Now we need to do some design.  What's the easiest way to fix this?  The most direct would be to remove all spaces from the text and then do a comparison.  Let's try that:</p>
<pre><code class="language-csharp">        public bool Analyze(string a, string b)
        {
            string suba = a.Replace(&quot; &quot;, &quot;&quot;);
            string subb = b.Replace(&quot; &quot;, &quot;&quot;);

            return suba == subb;
        }
</code></pre>
<p>Again, we've made progress.  All tests pass!</p>
<p>The next few tests we'll tackle at the same time.  We don't have to do this one test at a time.  Here are the tests:</p>
<pre><code class="language-csharp">        [Fact]
        public void SameTextWithTabs_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze(&quot;This\t is\t\ta\ttest&quot;, &quot;\tThis is a test\t\t&quot;);

            Assert.True(result, &quot;The result should be true for the same words in the text.&quot;);
        }

        [Fact]
        public void SameTextWithNewlines_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze(&quot;This\nis\na\ntest&quot;, &quot;\nThis is a test\n\n&quot;);

            Assert.True(result, &quot;The result should be true for the same words in the text.&quot;);
        }

        [Fact]
        public void SameTextWithCarriageReturns_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze(&quot;This\ris a\rtest&quot;, &quot;\r\rThis is a test\r&quot;);

            Assert.True(result, &quot;The result should be true for the same words in the text.&quot;);
        }

        [Fact]
        public void SameTextWithDifferentPunctuation_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze(&quot;This is a test!&quot;, &quot;This is a test?&quot;);

            Assert.True(result, &quot;The result should be true for the same words in the text.&quot;);
        }
</code></pre>
<p>Some may say that you should not introduce many tests at once.  And that is a rule worth breaking from time to time.  In this case I am using it as a means of changing the design of the implementation.  Based on our previous success, we might evolve to the following as our current solution:</p>
<pre><code class="language-csharp">        public bool Analyze(string a, string b)
        {
            string suba = a.Replace(&quot; &quot;, &quot;&quot;).Replace(&quot;\n&quot;, &quot;&quot;).Replace(&quot;\r&quot;, &quot;&quot;).Replace(&quot;\t&quot;, &quot;&quot;).Replace(&quot;!&quot;, &quot;&quot;).Replace(&quot;?&quot;, &quot;&quot;);
            string subb = b.Replace(&quot; &quot;, &quot;&quot;).Replace(&quot;\n&quot;, &quot;&quot;).Replace(&quot;\r&quot;, &quot;&quot;).Replace(&quot;\t&quot;, &quot;&quot;).Replace(&quot;!&quot;, &quot;&quot;).Replace(&quot;?&quot;, &quot;&quot;);

            return suba == subb;
        }
</code></pre>
<p>The above code clearly followed the rule to only do the minimum work to make the test pass.  And, based on the tests provided thus far, we are passing all of our tests.  The problem is that the design will suffer from very slow performance and very high memory use.  Each call to <em>Replace()</em> will generate a new string.  And if you are trying to test two very long documents, you might run out of memory before you get your answer.  Besides, we have more tests that need to be developed.
It's always a good idea to perform a sanity check on the code you are writing.  We can assume that additional tests for ignoring special characters can be written and we will need to pass them.  So, a better idea might be to identify only those characters we need to keep for our analysis.  So, let's make that change to only look for valid characters.</p>
<pre><code class="language-csharp">        public bool Analyze(string a, string b)
        {
            var docA = new DocInfo(a);
            var docB = new DocInfo(b);

            return docA.ToString() == docB.ToString();
        }
</code></pre>
<p>As you can see, I introduced a new class, <em>DocInfo</em>, to process the string provided so that a comparison can be made.  Here's the code for the <em>DocInfo</em> class:</p>
<pre><code class="language-csharp">    public class DocInfo
    {
        private readonly string _parsedString;

        public DocInfo(string src)
        {
            _parsedString = ParseDocument(src);
        }

        private string ParseDocument(string src)
        {
            var sb = new StringBuilder();

            foreach (char c in src)
            {
                if ((c &gt;= 'a' &amp;&amp; c &lt;= 'z')
                    || (c &gt;= 'A' &amp;&amp; c &lt;= 'Z'))
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }

        public override string ToString()
        {
            return _parsedString;
        }
    }
</code></pre>
<p>The function <em>ParseDocument()</em> in the <em>DocInfo</em> class is the meat of the class right now.  It does the job of extracting only the characters we need to process, the letters A-Z and a-z.  We will get to the part where capitalization is not important later.  But now the <em>DocInfo</em> class is helping the analyzer by performing a single job, which is to extract out the data we need.
All tests are still passing.  So, one might ask, why are we not counting words, like in that word histrogram thing you were talking about at the beginning?  Fair question!  The reasons is that we really haven't needed to yet, based solely on the tests provided.  A more strenuous test may have forced our hand earlier on, but it just wasn't necessary.</p>
<p>We will introduce a test now that will point us in the right direction with regard to word identification and analysis:</p>
<pre><code class="language-csharp">        [Fact]
        public void ChangesOfMinorWords_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            string paragraphA = &quot;Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation.&quot;;
            string paragraphB = &quot;Some years ago, never mind how long precisely, and having little to no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It's a way I have of driving off of the spleen, and regulating the circulation. &quot;;

            bool result = a.Analyze(paragraphA, paragraphB);

            Assert.True(result, &quot;When only minor words are changed, the content has still been copied.&quot;);
        }
</code></pre>
<p>Now it's getting interesting!  The test is comparing some text from the opening paragraph of <strong>Moby Dick</strong>, by <em>Herman Melville</em>.  The text being compared has been slightly altered by changing just a few small words.
Our solution will simply be to ignore words that impart little to no meaning to a sentence.
We'll need to capture words though, and that is what the next piece of code will be doing:</p>
<pre><code class="language-csharp">    public class DocInfo
    {
        private readonly string _parsedString;
        private readonly Dictionary&lt;string, string&gt; _smallWords = new Dictionary&lt;string, string&gt;
        {
            {&quot;the&quot;, &quot;the&quot;},
            {&quot;and&quot;, &quot;and&quot;},
            {&quot;but&quot;, &quot;but&quot;},
            {&quot;or&quot;, &quot;or&quot;},
            {&quot;re&quot;, &quot;re&quot; },
            {&quot;it&quot;, &quot;it&quot;},
            {&quot;is&quot;, &quot;is&quot;},
            {&quot;not&quot;, &quot;not&quot;},
            {&quot;you&quot;, &quot;you&quot;},
            {&quot;your&quot;, &quot;your&quot; },
            {&quot;they&quot;, &quot;they&quot;},
            {&quot;theyre&quot;, &quot;theyre&quot;},
            {&quot;are&quot;, &quot;are&quot;},
            {&quot;youre&quot;, &quot;youre&quot;},
            {&quot;as&quot;, &quot;as&quot;},
            {&quot;in&quot;, &quot;in&quot;},
            {&quot;of&quot;, &quot;of&quot;},
            {&quot;if&quot;, &quot;if&quot;},
            {&quot;to&quot;, &quot;to&quot; },
            {&quot;too&quot;, &quot;too&quot; },
            {&quot;two&quot;, &quot;two&quot; }
        };

        public DocInfo(string src)
        {
            _parsedString = ParseDocument(src);
        }

        private string ParseDocument(string src)
        {
            var content = new StringBuilder();
            var currWord = new StringBuilder();

            foreach (char c in src)
            {
                if ((c &gt;= 'a' &amp;&amp; c &lt;= 'z')
                    || (c &gt;= 'A' &amp;&amp; c &lt;= 'Z'))
                {
                    currWord.Append(c);
                }
                else
                {
                    // only words with 2 or more characters are going to be possibly meaningful
                    if (currWord.Length &gt; 1 &amp;&amp; !_smallWords.ContainsKey(currWord.ToString()))
                    {
                        content.Append(currWord);
                    }

                    currWord.Clear();
                }
            }

            if (currWord.Length &gt; 0 &amp;&amp; !_smallWords.ContainsKey(currWord.ToString()))
            {
                content.Append(currWord);
            }

            return content.ToString();
        }

        public override string ToString()
        {
            return _parsedString;
        }
    }
</code></pre>
<p>I dropped the initial <em>sb</em> in favor of the name <em>content</em>.  I added <em>currWord</em> to keep track of the current <em>word</em> token we were extracting from the string.  In order for <em>currWord</em> to be a valid candidate for <em>content</em>, it must be 2 or more characters in length, and must not be in the list of words or word fragments to be skipped.
Now all tests pass.  For full disclosure, the test for <em>currWord</em> outside the main loop was added after some other tests were broken!  Seems I had forgotten we could come to the end of a string without any punctuation!</p>
<p>I'm going to introduce a change to the last test to see if we can accommodate differences in case.</p>
<pre><code class="language-csharp">        [Fact]
        public void ChangesOfCase_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            string paragraphA = &quot;Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation.&quot;;
            string paragraphB = &quot;And, some years ago. Never mind how long precisely. Having little to no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It's a way I have of driving off of the spleen. Regulating the circulation. &quot;;

            bool result = a.Analyze(paragraphA, paragraphB);

            Assert.True(result, &quot;When only minor words are changed, the content has still been copied.&quot;);
        }
</code></pre>
<p>And the test fails.  Should be simple to address this though.  Let's try the following:</p>
<pre><code class="language-csharp">        private string ParseDocument(string src)
        {
            var content = new StringBuilder();
            var currWord = new StringBuilder();

            foreach (char c in src)
            {
                if (c &gt;= 'a' &amp;&amp; c &lt;= 'z')
                {
                    currWord.Append(c);
                }
                else if (c &gt;= 'A' &amp;&amp; c &lt;= 'Z')
                {
                    currWord.Append((char)('a' + (c - 'A')));
                }
                else
                {
                    // only words with 2 or more characters are going to be possibly meaningful
                    if (currWord.Length &gt; 1 &amp;&amp; !_smallWords.ContainsKey(currWord.ToString()))
                    {
                        content.Append(currWord);
                    }

                    currWord.Clear();
                }
            }

            if (currWord.Length &gt; 0 &amp;&amp; !_smallWords.ContainsKey(currWord.ToString()))
            {
                content.Append(currWord);
            }

            return content.ToString();
        }
</code></pre>
<p>Changing the <em>ParseDocument()</em> function we are converting the uppercase characters directly to lowercase with just a little character math.  That's pretty simple.</p>
<p>Since we have some similar logic repeated in <em>ParseDocument()</em>, let's do a bit of refactoring.  Look at it now:</p>
<pre><code class="language-csharp">        private string ParseDocument(string src)
        {
            var content = new StringBuilder();
            var currWord = new StringBuilder();

            foreach (char c in src)
            {
                if (!CharacterAdded(c, currWord))
                {
                    // only words with 2 or more characters are going to be possibly meaningful
                    AddWord(content, currWord);

                    currWord.Clear();
                }
            }

            AddWord(content, currWord);

            return content.ToString();
        }

        private bool CharacterAdded(char c, StringBuilder word)
        {
            if (c &gt;= 'a' &amp;&amp; c &lt;= 'z')
            {
                word.Append(c);
                return true;
            }
            else if (c &gt;= 'A' &amp;&amp; c &lt;= 'Z')
            {
                word.Append((char)('a' + (c - 'A')));
                return true;
            }

            return false;
        }

        private void AddWord(StringBuilder content, StringBuilder word)
        {
            if (word.Length &gt; 1 &amp;&amp; !_smallWords.ContainsKey(word.ToString()))
            {
                content.Append(word);
            }
        }
</code></pre>
<p>Now the <em>ParseDocument()</em> function has been refactored and is much easier to read.  The components for adding a character to the current word were moved to the <em>CharacterAdded()</em> function, and the appending of the word candidate for <em>currWord</em> to the <em>content</em> variable have been moved to the <em>AddWord()</em> method.</p>
<p>The mechanism for detecting copied works has been rather weak in that we are only really looking for an <em>exact</em> kind of sameness.  This is born out in our examination of the source documents and extracting their <em>content</em> by removing punctuation and ignoring less significant words and character case.  But we are still just comparing two resultant strings, for <strong>exact</strong> sameness.  It is now time to raise the bar, so to speak, by injecting some new text into an entire working paragraph.  Let's see what happens when we try the following test:</p>
<pre><code class="language-csharp">        [Fact]
        public void InjectedText_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            string paragraphA = &quot;Call me Ishmael. Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation. Whenever I find myself growing grim about the mouth; whenever it is a damp, drizzly November in my soul; whenever I find myself involuntarily pausing before coffin warehouses, and bringing up the rear of every funeral I meet; and especially whenever my hypos get such an upper hand of me, that it requires a strong moral principle to prevent me from deliberately stepping into the street, and methodologically knocking people's hats off--then, I account it high time to get to sea as soon as I can. This is my substitute for pistol and ball... I quietly take to the ship. There is nothing surprising in this. If they but knew it, almost all men in their degree, some time or other, cherish very nearly the same feelings towards the ocean with me.&quot;;
            string paragraphB = &quot;Call me Ishmael. Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation. Whenever I find myself growing grim about the mouth; whenever it is a damp, drizzly November in my soul; whenever I find myself involuntarily pausing before coffin warehouses, and bringing up the rear of every funeral I meet; and especially whenever my hypos get such an upper hand of me, that it requires a strong moral principle to prevent me from deliberately stepping into the street, and methodologically knocking people's hats off--then, I account it high time to get to sea as soon as I can.  I can feel it in my very bones. This is my substitute for pistol and ball... I quietly take to the ship. There is nothing surprising in this. If they but knew it, almost all men in their degree, some time or other, cherish very nearly the same feelings towards the ocean with me.&quot;;

            bool result = a.Analyze(paragraphA, paragraphB);

            Assert.True(result, &quot;When the words are essentially the same, some of the content has been copied.&quot;);
        }
</code></pre>
<p>With the addition of the line &quot;I can feel it in my very bones.&quot;, the test now fails.  The one writing the test clearly things that the test should not fail.  That, in fact, the analyzer should detect that there has been some plagiarism.  Now we need to prove it.
Now we need to begin looking at words.  If the same words are used, with the same frequency, in two different documents, then one has been most certainly copied.
Until now we have been relying on <em>DocInfo</em> to prepare the analysis for the <em>Analyze</em> function.  We will be making some changes to <em>DocInfo</em> to collect information on the documents, but the analyzer will be modified to perform the actual analysis.  It has, thus far, only compared two strings.</p>
<pre><code class="language-csharp">    public class DocInfo
    {
        private readonly Dictionary&lt;string, int&gt; _wordCounts = new Dictionary&lt;string, int&gt;();

        private void AddWord(StringBuilder content, StringBuilder word)
        {
            if (word.Length &gt; 1 &amp;&amp; !_smallWords.ContainsKey(word.ToString()))
            {
                string w = word.ToString();

                if (_wordCounts.ContainsKey(w))
                {
                    _wordCounts[w]++;  // increment count by 1
                }
                else
                {
                    _wordCounts.Add(w, 1); // initialize to count of 1
                }

                content.Append(w);
            }
        }

        public Dictionary&lt;string, int&gt; WordCounts
        {
            get { return _wordCounts; }
        }
    }
</code></pre>
<p>As you can see, the <em>DocInfo</em> class has gone through some slight changes (Only the changed content is being shown here).  It's now collecting word information for a document.  That information is being exposed in a dictionary of words and counts in the <em>WordCounts</em> property.
Running the new test will show that the resulting word count has 112 unique words in the original paragraph and 114 in the second one.  The analyzer will shake that down to determine if there is sufficient evidence to suggest some plagiarism has occurred.</p>
<p>The following changes have been made to the <em>PlagiarismAnalyzer</em> class:</p>
<pre><code class="language-csharp">    public class PlagiarismAnalyzer
    {
        public bool Analyze(string a, string b)
        {
            var docA = new DocInfo(a);
            var docB = new DocInfo(b);

            List&lt;string&gt; wordsA = new List&lt;string&gt;(docA.WordCounts.Keys);
            wordsA.Sort();
            List&lt;string&gt; wordsB = new List&lt;string&gt;(docB.WordCounts.Keys);
            wordsB.Sort();

            // we want to count the total number of word instances in B that are not in A
            // we want to also count the total number of word instances in B.
            // we want to also know the total number of word instances in A.

            string currentWord;
            int wordsCounted = 0;
            int totalWords = wordsA.Count + wordsB.Count;
            int idxA = 0;
            int idxB = 0;

            int wordCountA = 0;
            int wordCountB = 0;
            int uniqueWordsB = 0;

            currentWord = GetCurrentWord(wordsA, wordsB, idxA, idxB);

            while (wordsCounted &lt; totalWords)
            {
                int aCount = 0;

                if (idxA &lt; wordsA.Count &amp;&amp; currentWord == wordsA[idxA])
                {
                    wordCountA += docA.WordCounts[currentWord]; // the count of words in A
                    aCount = docA.WordCounts[currentWord];
                    wordsCounted++;
                    idxA++;
                }

                if (idxB &lt; wordsB.Count &amp;&amp; currentWord == wordsB[idxB])
                {
                    int bCount = docB.WordCounts[currentWord];

                    wordCountB += bCount; // count of words in B

                    if (bCount &gt; aCount)
                    {
                        uniqueWordsB += (docB.WordCounts[currentWord] - aCount);
                    }
                    wordsCounted++;
                    idxB++;
                }

                currentWord = GetCurrentWord(wordsA, wordsB, idxA, idxB);
            }

            return (float)uniqueWordsB / (float)wordCountB &lt;= 0.5f;
        }

        private string GetCurrentWord(List&lt;string&gt; a, List&lt;string&gt; b, int idxA, int idxB)
        {
            string currentWord = null;

            if (idxA &gt;= a.Count &amp;&amp; idxB &gt;= b.Count)
            {
                return null;
            }

            if (idxA &gt;= a.Count)
            {
                return b[idxB];
            }

            if (idxB &gt;= b.Count)
            {
                return a[idxA];
            }

            if (a[idxA].CompareTo(b[idxB]) &lt;= 0)
            {
                currentWord = a[idxA];
            }
            else
            {
                currentWord = b[idxB];
            }

            return currentWord;
        }
    }
</code></pre>
<p>In the <em>Analyze</em> function we are now pondering the differences between the <em>WordCount</em> properties of the two <em>DocInfo</em> instances.  What is essentially happening is that a staggered walk through the sorted lists of words for each document is taking place.  The total instance count of words for each document is being calculated and the unique word instances in document B are being calculated.  If the ratio of unique words in B is less than 50%, we can certify that at least <strong>some</strong> of A was copied.  We could probably go higher than that.<br />
The helper method for the staggered walk between the sorted lists is the <em>GetCurrentWord()</em> function.  Its job is to select the next word in sorted order between the two lists based on their current positions.
For the last test, only 5 unique word instances out of a total of 137 were unique to document B.  Which figures nicely with our understanding of the texts.</p>
<p>This concludes this first introduction to test driven development (TDD).  Hopefully you found it interesting and educational.</p>
<p>If you are interested in the subject of plagiarism consider a few things we discovered.  A single paragraph may only have 130 or so unique and meaningful words in it.  Perhaps even less, as I didn't attempt to provide a very complete list of them.  An entire novel may only have about 3500 unique words.  That's not a lot of data.  One of the nice things about our implementation is that the <em>DocInfo</em> class can be used standalone to record the word count list for any document.  (You can modify it to read a file if you like!)
A collection of word lists can be saved in a database for later retrieval.  Consider that a university could very easily store a 40 KB file for every paper ever submitted.  If you imagine a single department collecting 30,000 papers every year, that would be only 1.2 MB of data.  Over 50 years, that is barely 60 MB of data!  It would be trivial to scan in your paper, do the calculations, and compare to all of the entries that came before.  It would be all too easy to find that you directly used an essay from decades before as your own.</p>
<p>Consider some more ramifications of this type of analysis.  What if we captured this type of information from smaller parts of a document, say a speech.  Every single line and paragraph could be processed in much the same manner and compared to speeches given years, decades, or millenia in the past.  Politicians, and the speech writers they rely on, have been recently caught reusing the words of others.  With modern technology it is much more difficult to get away with.</p>
<p>Thank you for reading this.  It was a labor of love and I hope it serves you well.  Please don't hesitate to leave a comment.</p>
<p>Thanks!</p>
<p>Bob Briggs
7/21/2018</p>


</body>
</html>