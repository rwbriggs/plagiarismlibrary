using PlagiarismLibrary;
using System;
using Xunit;

namespace PlagiarismLibraryTests
{
    public class AnalyzerTests
    {
        [Fact]
        public void SameText_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This is a test", "This is a test");

            Assert.True(result, "The result should be true for the same text.");
        }

        [Fact]
        public void DifferentText_ReturnsFalse()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This test should always fail.", "Indeed, we like this one.");

            Assert.False(result, "The different text should always fail.");
        }

        [Fact]
        public void SameTextWithSpaces_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This  is   a   test", "  This is a test   ");

            Assert.True(result, "The result should be true for the same words in the text.");
        }

        [Fact]
        public void SameTextWithTabs_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This\t is\t\ta\ttest", "\tThis is a test\t\t");

            Assert.True(result, "The result should be true for the same words in the text.");
        }

        [Fact]
        public void SameTextWithNewlines_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This\nis\na\ntest", "\nThis is a test\n\n");

            Assert.True(result, "The result should be true for the same words in the text.");
        }

        [Fact]
        public void SameTextWithCarriageReturns_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This\ris a\rtest", "\r\rThis is a test\r");

            Assert.True(result, "The result should be true for the same words in the text.");
        }

        [Fact]
        public void SameTextWithDifferentPunctuation_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This is a test!", "This is a test?");

            Assert.True(result, "The result should be true for the same words in the text.");
        }

        [Fact]
        public void ChangesOfMinorWords_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            string paragraphA = "Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation.";
            string paragraphB = "Some years ago, never mind how long precisely, and having little to no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It's a way I have of driving off of the spleen, and regulating the circulation. ";

            bool result = a.Analyze(paragraphA, paragraphB);

            Assert.True(result, "When only minor words are changed, the content has still been copied.");
        }

        [Fact]
        public void ChangesOfCase_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            string paragraphA = "Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation.";
            string paragraphB = "And, some years ago. Never mind how long precisely. Having little to no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It's a way I have of driving off of the spleen. Regulating the circulation. ";

            bool result = a.Analyze(paragraphA, paragraphB);

            Assert.True(result, "When only minor words are changed, the content has still been copied.");
        }

        [Fact]
        public void InjectedText_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            string paragraphA = "Call me Ishmael. Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation. Whenever I find myself growing grim about the mouth; whenever it is a damp, drizzly November in my soul; whenever I find myself involuntarily pausing before coffin warehouses, and bringing up the rear of every funeral I meet; and especially whenever my hypos get such an upper hand of me, that it requires a strong moral principle to prevent me from deliberately stepping into the street, and methodologically knocking people's hats off--then, I account it high time to get to sea as soon as I can. This is my substitute for pistol and ball... I quietly take to the ship. There is nothing surprising in this. If they but knew it, almost all men in their degree, some time or other, cherish very nearly the same feelings towards the ocean with me.";
            string paragraphB = "Call me Ishmael. Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation. Whenever I find myself growing grim about the mouth; whenever it is a damp, drizzly November in my soul; whenever I find myself involuntarily pausing before coffin warehouses, and bringing up the rear of every funeral I meet; and especially whenever my hypos get such an upper hand of me, that it requires a strong moral principle to prevent me from deliberately stepping into the street, and methodologically knocking people's hats off--then, I account it high time to get to sea as soon as I can.  I can feel it in my very bones. This is my substitute for pistol and ball... I quietly take to the ship. There is nothing surprising in this. If they but knew it, almost all men in their degree, some time or other, cherish very nearly the same feelings towards the ocean with me.";

            bool result = a.Analyze(paragraphA, paragraphB);

            Assert.True(result, "When the words are essentially the same, some of the content has been copied.");
        }

    }
}
