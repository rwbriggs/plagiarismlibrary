# PlagiarismLibrary

### Using Test Driven Development (TDD)

The basic concept of TDD is that you first design a test for the code you have yet to write.  That test should fail. Then you write the actual code that will pass the test.  You continue to add capabilities to your project by first designing the capability, then implement the tests to validate, and finally write the code to pass the tests.  Sounds simple, right?

Here we go.

We will develop a library to determine if the content of one text file is the same as the content of another text file.  The resulting application will be able to detect if one article is plagiarized off of another.  In order to do this, we will need to parse out the unique words and word count for a body of text, ignoring filler words, to generate a histogram of the word counts.  We'll compare this to the histogram for another body of text and use a scoring system to determine if there is a likelihood of plagiarism.

It will be the library we develop the unit tests for.  In the end, we will create a simple console application to see if we succeeded.

I will be using Visual Studio 2017 and C# for this demonstration.  The same principles will apply in any language with support for unit tests.  I will call out the differences as they become apparent to me.  As we go through this process you will likely see that we will have to change our implementation as we introduce new capabilities to the library.  This is normal behavior for TDD, and, in truth, for most development practices.  The point to be made here is that change is something to be embraced and not feared.

So, first steps.  We'll start with a project for a .Net Core library called PlagiarismLibrary.  The main class for that library will be PlagiarismAnalyzer, which will be our main entry point for comparing text and determining if something has just been copied.  So we start with the class as follows:


```csharp
    public class PlagiarismAnalyzer
    {
    }
```

No, it doesn't do anything yet.  That's fine.  What we need to do is to create the unit test library, so we'll add the PlagiarismLibraryTests XUnit testing project.  In it we'll create our first test class called AnalyzerTests and this is where we'll start.


```csharp
    public class AnalyzerTests
    {
    }
```

We are still doing a whole lot of nothing so far.  Let's create a test.  The main, basic test will be to make sure we indicate that 2 strings are equal when they are, indeed, equal.  So let's add the following test:

```csharp
    public class AnalyzerTests
    {
        [Fact]
        public void SameText_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This is a test", "This is a test");

            Assert.True(result, "The result should be true for the same text.");
        }
    }
```

Our first test ('Fact' in XUnit) will analyze 2 strings that are identical and expect a __true__ response.  This makes sense, right?  But it won't build and the editor will complain that the function Analyze doesn't exist.  One of the parts of TDD is that you only create what you need.  The test will provide the design.  So, let's build the Analyze method!  This is what the interface should look like:

```csharp
    public class PlagiarismAnalyzer
    {
        public bool Analyze(string a, string b)
        {
        }
    }
```

We have not returned a value, so it won't compile.  Let's return the default value for a Boolean.  So, we'll have this:

```csharp
    public class PlagiarismAnalyzer
    {
        public bool Analyze(string a, string b)
        {
            return default(bool);
        }
    }
```

Let's run the test.  It fails.  Which is what we expected, as the default value of a Boolean is false.  So, we need to fix our code to pass the test.  This is our first opportunity to do something!  While we __can__ actually compare the strings, that would be too much work.  The goal of TDD is to do the __minimum__ amount of work in order to pass the tests.  So we'll change the Analyze() function to:

```csharp
        public bool Analyze(string a, string b)
        {
            return true;
        }
```

Now, when we run the test it passes.

We added the first test and it passes.  That's a good start!  However, if we create a second test that is supposed to fail, our code will not pass.  The new test needs to make sure that different strings will return false.  Here's the new test:

```csharp
        [Fact]
        public void DifferentText_ReturnsFalse()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This test should always fail.", "Indeed, we like this one.");

            Assert.False(result, "The different text should always fail.");
        }
```

However, when we run our tests we have a failure.  We knew that was going to happen, right?  So, how do we fix it?  Well, it seems we need to compare the string values.  That's pretty easy to do.

```csharp
        public bool Analyze(string a, string b)
        {
            return a == b;
        }
```

Now, when we run our tests they pass.  It's a nice start.  We are not done yet.  We only know that if two strings are identical will they be flagged as plagiarized copies.  We need to up our game.  The next test is to insert some extra spaces in the text to be tested.  The expectation is that it should still return true.

```csharp
        [Fact]
        public void SameTextWithSpaces_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This  is   a   test", "  This is a test   ");

            Assert.True(result, "The result should be true for the same words in the text.");
        }
```

This test will fail.  Now we need to do some design.  What's the easiest way to fix this?  The most direct would be to remove all spaces from the text and then do a comparison.  Let's try that:

```csharp
        public bool Analyze(string a, string b)
        {
            string suba = a.Replace(" ", "");
            string subb = b.Replace(" ", "");

            return suba == subb;
        }
```

Again, we've made progress.  All tests pass!

The next few tests we'll tackle at the same time.  We don't have to do this one test at a time.  Here are the tests:

```csharp
        [Fact]
        public void SameTextWithTabs_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This\t is\t\ta\ttest", "\tThis is a test\t\t");

            Assert.True(result, "The result should be true for the same words in the text.");
        }

        [Fact]
        public void SameTextWithNewlines_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This\nis\na\ntest", "\nThis is a test\n\n");

            Assert.True(result, "The result should be true for the same words in the text.");
        }

        [Fact]
        public void SameTextWithCarriageReturns_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This\ris a\rtest", "\r\rThis is a test\r");

            Assert.True(result, "The result should be true for the same words in the text.");
        }

        [Fact]
        public void SameTextWithDifferentPunctuation_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            bool result = a.Analyze("This is a test!", "This is a test?");

            Assert.True(result, "The result should be true for the same words in the text.");
        }
```

Some may say that you should not introduce many tests at once.  And that is a rule worth breaking from time to time.  In this case I am using it as a means of changing the design of the implementation.  Based on our previous success, we might evolve to the following as our current solution:

```csharp
        public bool Analyze(string a, string b)
        {
            string suba = a.Replace(" ", "").Replace("\n", "").Replace("\r", "").Replace("\t", "").Replace("!", "").Replace("?", "");
            string subb = b.Replace(" ", "").Replace("\n", "").Replace("\r", "").Replace("\t", "").Replace("!", "").Replace("?", "");

            return suba == subb;
        }
```
The above code clearly followed the rule to only do the minimum work to make the test pass.  And, based on the tests provided thus far, we are passing all of our tests.  The problem is that the design will suffer from very slow performance and very high memory use.  Each call to _Replace()_ will generate a new string.  And if you are trying to test two very long documents, you might run out of memory before you get your answer.  Besides, we have more tests that need to be developed.
It's always a good idea to perform a sanity check on the code you are writing.  We can assume that additional tests for ignoring special characters can be written and we will need to pass them.  So, a better idea might be to identify only those characters we need to keep for our analysis.  So, let's make that change to only look for valid characters.

```csharp
        public bool Analyze(string a, string b)
        {
            var docA = new DocInfo(a);
            var docB = new DocInfo(b);

            return docA.ToString() == docB.ToString();
        }
```

As you can see, I introduced a new class, _DocInfo_, to process the string provided so that a comparison can be made.  Here's the code for the _DocInfo_ class:

```csharp
    public class DocInfo
    {
        private readonly string _parsedString;

        public DocInfo(string src)
        {
            _parsedString = ParseDocument(src);
        }

        private string ParseDocument(string src)
        {
            var sb = new StringBuilder();

            foreach (char c in src)
            {
                if ((c >= 'a' && c <= 'z')
                    || (c >= 'A' && c <= 'Z'))
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }

        public override string ToString()
        {
            return _parsedString;
        }
    }
```

The function _ParseDocument()_ in the _DocInfo_ class is the meat of the class right now.  It does the job of extracting only the characters we need to process, the letters A-Z and a-z.  We will get to the part where capitalization is not important later.  But now the _DocInfo_ class is helping the analyzer by performing a single job, which is to extract out the data we need.
All tests are still passing.  So, one might ask, why are we not counting words, like in that word histrogram thing you were talking about at the beginning?  Fair question!  The reasons is that we really haven't needed to yet, based solely on the tests provided.  A more strenuous test may have forced our hand earlier on, but it just wasn't necessary.

We will introduce a test now that will point us in the right direction with regard to word identification and analysis:

```csharp
        [Fact]
        public void ChangesOfMinorWords_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            string paragraphA = "Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation.";
            string paragraphB = "Some years ago, never mind how long precisely, and having little to no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It's a way I have of driving off of the spleen, and regulating the circulation. ";

            bool result = a.Analyze(paragraphA, paragraphB);

            Assert.True(result, "When only minor words are changed, the content has still been copied.");
        }
```

Now it's getting interesting!  The test is comparing some text from the opening paragraph of __Moby Dick__, by _Herman Melville_.  The text being compared has been slightly altered by changing just a few small words.
Our solution will simply be to ignore words that impart little to no meaning to a sentence.
We'll need to capture words though, and that is what the next piece of code will be doing:

```csharp
    public class DocInfo
    {
        private readonly string _parsedString;
        private readonly Dictionary<string, string> _smallWords = new Dictionary<string, string>
        {
            {"the", "the"},
            {"and", "and"},
            {"but", "but"},
            {"or", "or"},
            {"re", "re" },
            {"it", "it"},
            {"is", "is"},
            {"not", "not"},
            {"you", "you"},
            {"your", "your" },
            {"they", "they"},
            {"theyre", "theyre"},
            {"are", "are"},
            {"youre", "youre"},
            {"as", "as"},
            {"in", "in"},
            {"of", "of"},
            {"if", "if"},
            {"to", "to" },
            {"too", "too" },
            {"two", "two" }
        };

        public DocInfo(string src)
        {
            _parsedString = ParseDocument(src);
        }

        private string ParseDocument(string src)
        {
            var content = new StringBuilder();
            var currWord = new StringBuilder();

            foreach (char c in src)
            {
                if ((c >= 'a' && c <= 'z')
                    || (c >= 'A' && c <= 'Z'))
                {
                    currWord.Append(c);
                }
                else
                {
                    // only words with 2 or more characters are going to be possibly meaningful
                    if (currWord.Length > 1 && !_smallWords.ContainsKey(currWord.ToString()))
                    {
                        content.Append(currWord);
                    }

                    currWord.Clear();
                }
            }

            if (currWord.Length > 0 && !_smallWords.ContainsKey(currWord.ToString()))
            {
                content.Append(currWord);
            }

            return content.ToString();
        }

        public override string ToString()
        {
            return _parsedString;
        }
    }
```

I dropped the initial _sb_ in favor of the name _content_.  I added _currWord_ to keep track of the current _word_ token we were extracting from the string.  In order for _currWord_ to be a valid candidate for _content_, it must be 2 or more characters in length, and must not be in the list of words or word fragments to be skipped.
Now all tests pass.  For full disclosure, the test for _currWord_ outside the main loop was added after some other tests were broken!  Seems I had forgotten we could come to the end of a string without any punctuation!

I'm going to introduce a change to the last test to see if we can accommodate differences in case.

```csharp
        [Fact]
        public void ChangesOfCase_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            string paragraphA = "Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation.";
            string paragraphB = "And, some years ago. Never mind how long precisely. Having little to no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It's a way I have of driving off of the spleen. Regulating the circulation. ";

            bool result = a.Analyze(paragraphA, paragraphB);

            Assert.True(result, "When only minor words are changed, the content has still been copied.");
        }
```

And the test fails.  Should be simple to address this though.  Let's try the following:

```csharp
        private string ParseDocument(string src)
        {
            var content = new StringBuilder();
            var currWord = new StringBuilder();

            foreach (char c in src)
            {
                if (c >= 'a' && c <= 'z')
                {
                    currWord.Append(c);
                }
                else if (c >= 'A' && c <= 'Z')
                {
                    currWord.Append((char)('a' + (c - 'A')));
                }
                else
                {
                    // only words with 2 or more characters are going to be possibly meaningful
                    if (currWord.Length > 1 && !_smallWords.ContainsKey(currWord.ToString()))
                    {
                        content.Append(currWord);
                    }

                    currWord.Clear();
                }
            }

            if (currWord.Length > 0 && !_smallWords.ContainsKey(currWord.ToString()))
            {
                content.Append(currWord);
            }

            return content.ToString();
        }
```

Changing the _ParseDocument()_ function we are converting the uppercase characters directly to lowercase with just a little character math.  That's pretty simple.

Since we have some similar logic repeated in _ParseDocument()_, let's do a bit of refactoring.  Look at it now:

```csharp
        private string ParseDocument(string src)
        {
            var content = new StringBuilder();
            var currWord = new StringBuilder();

            foreach (char c in src)
            {
                if (!CharacterAdded(c, currWord))
                {
                    // only words with 2 or more characters are going to be possibly meaningful
                    AddWord(content, currWord);

                    currWord.Clear();
                }
            }

            AddWord(content, currWord);

            return content.ToString();
        }

        private bool CharacterAdded(char c, StringBuilder word)
        {
            if (c >= 'a' && c <= 'z')
            {
                word.Append(c);
                return true;
            }
            else if (c >= 'A' && c <= 'Z')
            {
                word.Append((char)('a' + (c - 'A')));
                return true;
            }

            return false;
        }

        private void AddWord(StringBuilder content, StringBuilder word)
        {
            if (word.Length > 1 && !_smallWords.ContainsKey(word.ToString()))
            {
                content.Append(word);
            }
        }
```

Now the _ParseDocument()_ function has been refactored and is much easier to read.  The components for adding a character to the current word were moved to the _CharacterAdded()_ function, and the appending of the word candidate for _currWord_ to the _content_ variable have been moved to the _AddWord()_ method.

The mechanism for detecting copied works has been rather weak in that we are only really looking for an _exact_ kind of sameness.  This is born out in our examination of the source documents and extracting their _content_ by removing punctuation and ignoring less significant words and character case.  But we are still just comparing two resultant strings, for __exact__ sameness.  It is now time to raise the bar, so to speak, by injecting some new text into an entire working paragraph.  Let's see what happens when we try the following test:

```csharp
        [Fact]
        public void InjectedText_ReturnsTrue()
        {
            var a = new PlagiarismAnalyzer();

            string paragraphA = "Call me Ishmael. Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation. Whenever I find myself growing grim about the mouth; whenever it is a damp, drizzly November in my soul; whenever I find myself involuntarily pausing before coffin warehouses, and bringing up the rear of every funeral I meet; and especially whenever my hypos get such an upper hand of me, that it requires a strong moral principle to prevent me from deliberately stepping into the street, and methodologically knocking people's hats off--then, I account it high time to get to sea as soon as I can. This is my substitute for pistol and ball... I quietly take to the ship. There is nothing surprising in this. If they but knew it, almost all men in their degree, some time or other, cherish very nearly the same feelings towards the ocean with me.";
            string paragraphB = "Call me Ishmael. Some years ago--never mind how long precisely--having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen, and regulating circulation. Whenever I find myself growing grim about the mouth; whenever it is a damp, drizzly November in my soul; whenever I find myself involuntarily pausing before coffin warehouses, and bringing up the rear of every funeral I meet; and especially whenever my hypos get such an upper hand of me, that it requires a strong moral principle to prevent me from deliberately stepping into the street, and methodologically knocking people's hats off--then, I account it high time to get to sea as soon as I can.  I can feel it in my very bones. This is my substitute for pistol and ball... I quietly take to the ship. There is nothing surprising in this. If they but knew it, almost all men in their degree, some time or other, cherish very nearly the same feelings towards the ocean with me.";

            bool result = a.Analyze(paragraphA, paragraphB);

            Assert.True(result, "When the words are essentially the same, some of the content has been copied.");
        }
```

With the addition of the line "I can feel it in my very bones.", the test now fails.  The one writing the test clearly things that the test should not fail.  That, in fact, the analyzer should detect that there has been some plagiarism.  Now we need to prove it.
Now we need to begin looking at words.  If the same words are used, with the same frequency, in two different documents, then one has been most certainly copied.
Until now we have been relying on _DocInfo_ to prepare the analysis for the _Analyze_ function.  We will be making some changes to _DocInfo_ to collect information on the documents, but the analyzer will be modified to perform the actual analysis.  It has, thus far, only compared two strings.

```csharp
    public class DocInfo
    {
        private readonly Dictionary<string, int> _wordCounts = new Dictionary<string, int>();

        private void AddWord(StringBuilder content, StringBuilder word)
        {
            if (word.Length > 1 && !_smallWords.ContainsKey(word.ToString()))
            {
                string w = word.ToString();

                if (_wordCounts.ContainsKey(w))
                {
                    _wordCounts[w]++;  // increment count by 1
                }
                else
                {
                    _wordCounts.Add(w, 1); // initialize to count of 1
                }

                content.Append(w);
            }
        }

        public Dictionary<string, int> WordCounts
        {
            get { return _wordCounts; }
        }
    }
```

As you can see, the _DocInfo_ class has gone through some slight changes (Only the changed content is being shown here).  It's now collecting word information for a document.  That information is being exposed in a dictionary of words and counts in the _WordCounts_ property.
Running the new test will show that the resulting word count has 112 unique words in the original paragraph and 114 in the second one.  The analyzer will shake that down to determine if there is sufficient evidence to suggest some plagiarism has occurred.

The following changes have been made to the _PlagiarismAnalyzer_ class:

```csharp
    public class PlagiarismAnalyzer
    {
        public bool Analyze(string a, string b)
        {
            var docA = new DocInfo(a);
            var docB = new DocInfo(b);

            List<string> wordsA = new List<string>(docA.WordCounts.Keys);
            wordsA.Sort();
            List<string> wordsB = new List<string>(docB.WordCounts.Keys);
            wordsB.Sort();

            // we want to count the total number of word instances in B that are not in A
            // we want to also count the total number of word instances in B.
            // we want to also know the total number of word instances in A.

            string currentWord;
            int wordsCounted = 0;
            int totalWords = wordsA.Count + wordsB.Count;
            int idxA = 0;
            int idxB = 0;

            int wordCountA = 0;
            int wordCountB = 0;
            int uniqueWordsB = 0;

            currentWord = GetCurrentWord(wordsA, wordsB, idxA, idxB);

            while (wordsCounted < totalWords)
            {
                int aCount = 0;

                if (idxA < wordsA.Count && currentWord == wordsA[idxA])
                {
                    wordCountA += docA.WordCounts[currentWord]; // the count of words in A
                    aCount = docA.WordCounts[currentWord];
                    wordsCounted++;
                    idxA++;
                }

                if (idxB < wordsB.Count && currentWord == wordsB[idxB])
                {
                    int bCount = docB.WordCounts[currentWord];

                    wordCountB += bCount; // count of words in B

                    if (bCount > aCount)
                    {
                        uniqueWordsB += (docB.WordCounts[currentWord] - aCount);
                    }
                    wordsCounted++;
                    idxB++;
                }

                currentWord = GetCurrentWord(wordsA, wordsB, idxA, idxB);
            }

            return (float)uniqueWordsB / (float)wordCountB <= 0.5f;
        }

        private string GetCurrentWord(List<string> a, List<string> b, int idxA, int idxB)
        {
            string currentWord = null;

            if (idxA >= a.Count && idxB >= b.Count)
            {
                return null;
            }

            if (idxA >= a.Count)
            {
                return b[idxB];
            }

            if (idxB >= b.Count)
            {
                return a[idxA];
            }

            if (a[idxA].CompareTo(b[idxB]) <= 0)
            {
                currentWord = a[idxA];
            }
            else
            {
                currentWord = b[idxB];
            }

            return currentWord;
        }
    }
```

In the _Analyze_ function we are now pondering the differences between the _WordCount_ properties of the two _DocInfo_ instances.  What is essentially happening is that a staggered walk through the sorted lists of words for each document is taking place.  The total instance count of words for each document is being calculated and the unique word instances in document B are being calculated.  If the ratio of unique words in B is less than 50%, we can certify that at least __some__ of A was copied.  We could probably go higher than that.  
The helper method for the staggered walk between the sorted lists is the _GetCurrentWord()_ function.  Its job is to select the next word in sorted order between the two lists based on their current positions.
For the last test, only 5 unique word instances out of a total of 137 were unique to document B.  Which figures nicely with our understanding of the texts.

This concludes this first introduction to test driven development (TDD).  Hopefully you found it interesting and educational.

If you are interested in the subject of plagiarism consider a few things we discovered.  A single paragraph may only have 130 or so unique and meaningful words in it.  Perhaps even less, as I didn't attempt to provide a very complete list of them.  An entire novel may only have about 3500 unique words.  That's not a lot of data.  One of the nice things about our implementation is that the _DocInfo_ class can be used standalone to record the word count list for any document.  (You can modify it to read a file if you like!)
A collection of word lists can be saved in a database for later retrieval.  Consider that a university could very easily store a 40 KB file for every paper ever submitted.  If you imagine a single department collecting 30,000 papers every year, that would be only 1.2 MB of data.  Over 50 years, that is barely 60 MB of data!  It would be trivial to scan in your paper, do the calculations, and compare to all of the entries that came before.  It would be all too easy to find that you directly used an essay from decades before as your own.

Consider some more ramifications of this type of analysis.  What if we captured this type of information from smaller parts of a document, say a speech.  Every single line and paragraph could be processed in much the same manner and compared to speeches given years, decades, or millenia in the past.  Politicians, and the speech writers they rely on, have been recently caught reusing the words of others.  With modern technology it is much more difficult to get away with.

Thank you for reading this.  It was a labor of love and I hope it serves you well.  Please don't hesitate to leave a comment.

Thanks!

Bob Briggs
7/21/2018

















